#Population is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License

import pandas as pd
from pathlib import Path
import os



data_folder_path = Path(os.path.abspath(__file__))
data_folder_path = data_folder_path.parent / "data"

# Download the cities reference table from INSEE and put it in ./data/input/insee/recensement/ :
# https://www.insee.fr/fr/statistiques/5542859?sommaire=5395764#consulter-sommaire

year = "2016"

for file_n in ["FD_INDCVIZA_", "FD_INDCVIZB_", "FD_INDCVIZC_", "FD_INDCVIZD_", "FD_INDCVIZE_"]:
    file_name = file_n + year +".csv"
    print(file_name)
    file = pd.read_csv(os.path.join(data_folder_path, file_name), sep=";", dtype=str)
    file_name = file_n + year +".parquet"
    file.to_parquet(os.path.join(data_folder_path, file_name))



    
    
    
    
    


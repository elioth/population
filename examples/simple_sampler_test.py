#Population is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License

# -----------------------------
# Load packages
import pandas as pd
import sys
sys.path.append("..")
from population import Population
import numpy as np


# -----------------------------
# User input data
codgeo = "77046"
programme = pd.DataFrame({
    "building_id": "A",
    "n_rooms": [1, 2, 3, 4],
    "n_flats": [10, 5, 5, 5],
    "area_per_flat": [25, 40, 60, 80]
})
n_parking_spots = 8


# Run population sampling
n_housing = programme["n_flats"].sum()

pop = Population(codgeo=codgeo)
pop.sample(programme=programme, order=1, year="2018")

households = pop.households.copy()
individuals = pop.individuals.copy()


# -----------------------------
# Print sampling results
print("Number of housings :", n_housing)
print("Number of households :", households.shape[0])
print("Number of persons :", households["n_persons"].sum())

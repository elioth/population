Population
========================================
Population is a package developed by Elioth (https://elioth.com/) to :
- Get a sample of households representative of the population of a city and of a given housing programme (number of apartments x number of rooms per apartment).
- Get a sample of work places for the working individuals in the sampled population.

Large files are not included in this repository to keep it light. If you want to use the package please contact [Félix Pouchain](https://gitlab.com/FlxPo) to get a zipped copy of the necessary files.

License
---------------------

Population is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. 
 
Mobility is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with Mobility; If not, see <http://www.gnu.org/licenses/>.
 
@license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
See License file for detailled informations

=======
Copyright elioth

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.

You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.


Contributors
---------------------
- [Félix Pouchain](https://gitlab.com/FlxPo)
- [Louise Gontier](https://gitlab.com/l.gontier)

#Population is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License

import pandas as pd
from pathlib import Path
import os

def main():

    data_folder_path = Path(os.path.abspath(__file__))
    data_folder_path = data_folder_path.parent.parent / "data"
    
    # Download the cities reference table from INSEE and put it in ./data/input/insee/decoupage_communal/ :
    # https://www.insee.fr/fr/information/2028028
    com = pd.read_excel(
        data_folder_path / "input/insee/decoupage_communal/table-appartenance-geo-communes-20.xlsx",
        sheet_name="COM",
        skiprows=5
    )
    
    com = com.loc[~com["CODGEO"].isin(["75056", "69123", "13201"])]
    
    arm = pd.read_excel(
        data_folder_path / "input/insee/decoupage_communal/table-appartenance-geo-communes-20.xlsx",
        sheet_name="ARM",
        skiprows=5
    )
    
    com = pd.concat([com, arm])
    com = com[["CODGEO", "LIBGEO", "DEP", "REG", "CV"]]
    
    com.to_csv(data_folder_path / "output/cities.csv.gz", compression="gzip", encoding="utf-8", index=False)
    
    
if __name__ == "__main__":
    main()
